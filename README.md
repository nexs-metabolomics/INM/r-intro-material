# A practical introduction to R

Please find the reading material [here](https://nexs-metabolomics.gitlab.io/INM/r-intro-material/R_intro_material.html).

The slides used in the lecture can be found [here](https://nexs-metabolomics.gitlab.io/INM/r-intro-material//intro_to_R.html).
