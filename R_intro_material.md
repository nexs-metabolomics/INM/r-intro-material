---
title: "A practical introduction to R"
---

# A practical introduction to R

In this document I have collected some of the best online resources to give you a *brief* overview over how R works and a good vocabulary of basic functions. The goal is not to make you fluent in R but to make you understand the concepts well enough to make you able to *understand* and *modify* R code given to you. Therefore the material is of the fast and brief kind. You might feel that you do you have a firm grasp on what to do by going through this material. But that is OK for now.

The list is a combination of videos and texts freely available online and should be fairly fast to get through in a day or less. There is a focus on the so-called tidyverse which is a collection of tools with similar syntax that makes it easier to manipulate data to your needs, get it in a consistent format and makes your code easier to read.

I am using several of the free samples from [DataCamp](https://www.datacamp.com/) courses and I highly recommend continuing with their courses should you choose to increase your data analysis skills. Another good resource is the material from the [biostat2 courser at Université du Luxembourg](https://biostat2.uni.lu/lectures.html). RStudio also provides a number of [cheat sheets](https://rstudio.com/resources/cheatsheets/) such as "Data Transformation", "Data Visualization" and "Regular Expressions" that you can use as a reference.

<br/><br/>

<style type="text/css" rel="stylesheet">
img.book {
  content: url(images/large-book-template-hi3.png);
  height: 1em;
}
img.video {
  content: url(images/video-icon.png);
  height: 1em;
  margin-right: 1em;
}
img.ex {
  content: url(images/gym-icon-9.png);
  height: 1em;
  margin-right: 1em;
}
ol li {
padding-top: 1em;
}
ol li ul li{
padding-top: 0;
}
</style>


1. **RStudio and basic syntax**
   * <img class="video">[Getting oriented in Rstudio](https://www.youtube.com/watch?v=lTTJPRwnONE&list=PLLxj8fULvXwGOf8uHlL4Tr62oXSB5k_in&index=1) (9:27)
   * <img class="book"> R for Data Science: [Workflow: basics](https://r4ds.had.co.nz/workflow-basics.html)
   * <img class="book"> Introduction to R - for the scared but brave: [Basic syntax](https://stanstrup-obsolete.gitlab.io/Introduction-to-R/basic-syntax.html)
   * <img class="ex"> Introduction to R: [Intro to basics](https://learn.datacamp.com/courses/free-introduction-to-r)
2. **Object/Variable types**
   * <img class="video"> [Vectors, data frames, and lists](https://www.youtube.com/watch?v=XHAm_V-KZE8&list=PLLxj8fULvXwGOf8uHlL4Tr62oXSB5k_in&index=4) (7:47)
   * <img class="book"> Introduction to R - for the scared but brave: [Data classes/types](https://stanstrup-obsolete.gitlab.io/Introduction-to-R/data-classes-and-more-advanced-data-selection.html#data-classestypes)
3. **The pipe**
   * <img class="video"> [The pipe](https://www.youtube.com/watch?v=9yjhxvu-pDg&list=PLLxj8fULvXwGOf8uHlL4Tr62oXSB5k_in&index=3) (4:49)
   * <img class="book"> [Pipes in R](https://cfss.uchicago.edu/notes/pipes/) by Computing for the Social Sciences at the University of Chicago
4. **Tidyverse verbs and grouping**
   * <img class="video"> [The 5 verbs of dplyr](https://www.youtube.com/watch?v=sVISY_27znA&list=PLLxj8fULvXwGOf8uHlL4Tr62oXSB5k_in&index=7) (10:20)
   * <img class="video"><img class="ex"> [Introduction to the Tidyverse](https://learn.datacamp.com/courses/introduction-to-the-tidyverse)
   * <img class="book"> R for Data Science: [Data transformation](https://r4ds.had.co.nz/transform.html)
5. **Reshaping tables**
   * <img class="video"> [dplyr, or a dance with data (part 7)](https://www.youtube.com/watch?v=Hb3WdbivlkU) (*only* to 1:30 to 17:00)
   * <img class="book"> R for Data Science: [Tidy data](https://r4ds.had.co.nz/tidy-data.html) and [Pivoting](https://r4ds.had.co.nz/tidy-data.html#pivoting)
6. **Nested tables (tables inside tables) and "mapping"**
   * <img class="video"> [Map functions in purrr](https://www.youtube.com/watch?v=A8UaL47UXYE&list=PLLxj8fULvXwGOf8uHlL4Tr62oXSB5k_in&index=10&t=0s) (7:51)
   * <img class="book"> [Nest and map your way to efficient code](https://towardsdatascience.com/coding-in-r-nest-and-map-your-way-to-efficient-code-4e44ba58ee4a)
8. **filter with grepl or str_detect**
   * <img class="book"> [Combining dplyr::filter with grepl](https://nexs-metabolomics.gitlab.io/INM/r-intro-material/combine_filter_and_grep.html)
   * <img class="video"> [Learning R: filter by one or multiple strings using stringr and str_detect](https://www.youtube.com/watch?v=mDhzvO8gju8) (2:43) - Note that this uses `str_detect` instead of `grepl` but the point is the same
9. **ggplot2**
   * <img class="video"> [GGPLOT2 & DPLYR Enhanced Plots using gapminder Data](https://www.youtube.com/watch?v=EbZvcQy174M) (11:28)
   * <img class="book"> [Quick Introduction to ggplot2](https://bookdown.org/agrogankaylor/quick-intro-to-ggplot2/quick-intro-to-ggplot2.html) - In the upper right corner click "Code" --> "Show all code"
   * <img class="book"> [The Complete ggplot2 Tutorial - Part1](http://r-statistics.co/Complete-Ggplot2-Tutorial-Part1-With-R-Code.html)

